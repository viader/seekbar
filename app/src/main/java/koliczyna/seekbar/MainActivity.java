package koliczyna.seekbar;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import koliczyna.seekbar.views.CustomSeekBar;
import koliczyna.seekbar.views.CustomSeekBarDragListener;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        mainLayout.setOnDragListener(new CustomSeekBarDragListener());

        CustomSeekBar seekBar = new CustomSeekBar(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,50);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, seekBar.getId());
        seekBar.setLayoutParams(params);
        mainLayout.addView(seekBar);
    }
}
