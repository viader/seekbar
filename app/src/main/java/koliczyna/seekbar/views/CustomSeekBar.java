package koliczyna.seekbar.views;


import android.content.ClipData;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import koliczyna.seekbar.R;
import koliczyna.seekbar.utils.DeviceDimensionsHelper;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 15.06.15.
 */
public class CustomSeekBar extends View {

    private Context context;
    private int width;
    private int height;
    private float textSize;

    private Timer timer = new Timer();
    private Handler startDraHandler = new Handler();

    private int seconds;

    private static final int BACKGROUND_PADDING_VERTICAL = 10;
    private static final int BACKGROUND_PADDING_HORIZONTAL = 15;
    private static final int THUMB_WIDTH = BACKGROUND_PADDING_HORIZONTAL*2;

    private CustomSeekBarThumb thumb;
    private OnCustomSeekBarChangeListener listener;

    private Paint paintBackground = new Paint();
    private Paint paintProgress = new Paint();
    private Paint paintThumb = new Paint();
    private Paint paintText = new Paint();

    {
        paintBackground.setFlags(Paint.ANTI_ALIAS_FLAG);
        paintBackground.setAntiAlias(true);
        paintBackground.setStrokeWidth(0);
        paintBackground.setStyle(Paint.Style.FILL);
        paintBackground.setColor(0xffaeaeae);

        paintProgress.setFlags(Paint.ANTI_ALIAS_FLAG);
        paintProgress.setAntiAlias(true);
        paintProgress.setStrokeWidth(0);
        paintProgress.setStyle(Paint.Style.FILL);
        paintProgress.setColor(0xffff0000);

        paintThumb.setFlags(Paint.ANTI_ALIAS_FLAG);
        paintThumb.setAntiAlias(true);
        paintThumb.setStrokeWidth(0);
        paintThumb.setStyle(Paint.Style.FILL);
        paintThumb.setColor(0xffffffff);

        paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
        paintText.setAntiAlias(true);
        paintText.setColor(0xff000000);
    }

    public CustomSeekBar(Context context) {
        super(context);
        init(context, null);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setTimerColor(int color) {
        paintText.setColor(color);
    }

    public void setProgress(CustomSeekBarProgress progress) {
        thumb.setProgress(progress);
    }

    public void getProgress() {
        thumb.getProgress();
    }

    public OnCustomSeekBarChangeListener getListener() {
        return listener;
    }

    public void setListener(@NonNull OnCustomSeekBarChangeListener listener) {
        this.listener = listener;
    }

    private void init(Context context, AttributeSet attrs) {
        this.context = context;

        if(attrs!=null) {
            setAttributes(attrs);
        }

        textSize = DeviceDimensionsHelper.convertDpToPixel(10f, context);
        paintText.setTextSize(textSize);

        TimerTask timerTask = new UpdateViewTask();
        timer.scheduleAtFixedRate(timerTask, 0, 1 * 1000);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(width, height);
        sizeChanged();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        width = xNew;
        height = yNew;
        sizeChanged();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isInEditMode()) {
            return;
        }
        drawBackground(canvas);
        drawProgress(canvas);
        drawTimeText(canvas);
        drawThumb(canvas);
    }

    @Override
    public boolean onTouchEvent(@NonNull final MotionEvent event) {
        boolean superResult = super.onTouchEvent(event);
        getParent().requestDisallowInterceptTouchEvent(true);

        if(event.getActionMasked() == MotionEvent.ACTION_DOWN &&
                event.getX()>0 && event.getX()<width && event.getY()>0 && event.getY()<height) {
            thumb.setPushed(true);
        }

        if(thumb.isPushed()) {
            if (event.getActionMasked() == MotionEvent.ACTION_DOWN ||
                    event.getActionMasked() == MotionEvent.ACTION_MOVE) {
                if(event.getX()<0 || event.getX()>width || event.getY()<0 || event.getY()>height) {
                    onThumbDrop();
                }
                else if(thumb.getPosition()!=(int)event.getX()) {
                    startDraHandler.removeCallbacksAndMessages(null);
                    startDraHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ClipData data = ClipData.newPlainText("", "");
                            DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(CustomSeekBar.this);
                            CustomSeekBar.this.startDrag(data, shadowBuilder, CustomSeekBar.this, 0);
                            CustomSeekBar.this.setVisibility(View.INVISIBLE);
                        }
                    }, 1000);
                    thumb.setPosition((int) event.getX());
                    invalidate();
                }
                return true;
            }
            else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                onThumbDrop();
                return true;
            }
        }

        return superResult;
    }

    private void drawBackground(@NonNull Canvas canvas) {
        canvas.drawRect(BACKGROUND_PADDING_HORIZONTAL, BACKGROUND_PADDING_VERTICAL, width - BACKGROUND_PADDING_HORIZONTAL,
                height - BACKGROUND_PADDING_VERTICAL, paintBackground);
    }

    private void drawProgress(@NonNull Canvas canvas) {
        canvas.drawRect(BACKGROUND_PADDING_HORIZONTAL, BACKGROUND_PADDING_VERTICAL, thumb.getPosition(),
                height - BACKGROUND_PADDING_VERTICAL, paintProgress);
    }

    private void drawTimeText(@NonNull Canvas canvas) {
        canvas.drawText("" + seconds, THUMB_WIDTH + 5, (height + textSize) / 2, paintText);
    }

    private void drawThumb(@NonNull Canvas canvas) {
        thumb.draw(canvas, paintThumb);
    }

    private void sizeChanged() {
        thumb = new CustomSeekBarThumb(THUMB_WIDTH, height, width);
    }

    private void onThumbDrop() {
        thumb.setPushed(false);
        startDraHandler.removeCallbacksAndMessages(null);

        CustomSeekBarProgress oldProgress = thumb.getProgress();
        CustomSeekBarProgress newProgress = thumb.setProgressAndPosition();
        if(newProgress!=oldProgress) {
            seconds = 0;
            if(listener!=null) {
                listener.onProgressChanged(newProgress);
            }
        }
        invalidate();
    }

    private class UpdateViewTask extends TimerTask {
        @Override
        public void run() {
            seconds++;
            CustomSeekBar.this.postInvalidate();
        }
    }

    private void setAttributes(AttributeSet attrs) {
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomSeekBar);
        int backgroundColor = typedArray.getColor(R.styleable.CustomSeekBar_custom_seek_bar_background_color, 0xffaeaeae);
        int progressColor = typedArray.getColor(R.styleable.CustomSeekBar_custom_seek_bar_progress_color, 0xffff0000);
        int thumbColor = typedArray.getColor(R.styleable.CustomSeekBar_custom_seek_bar_thumb_color, 0xffffffff);
        int timerColor = typedArray.getColor(R.styleable.CustomSeekBar_custom_seek_bar_timer_color, 0xff000000);
        typedArray.recycle();
        paintBackground.setColor(backgroundColor);
        paintProgress.setColor(progressColor);
        paintThumb.setColor(thumbColor);
        paintText.setColor(timerColor);
    }

}
