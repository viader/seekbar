package koliczyna.seekbar.views;

import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 16.06.15.
 */
public class CustomSeekBarDragListener implements View.OnDragListener {
    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DROP:
                View view = (View) event.getLocalState();
                int x = (int)event.getX();
                int y = (int)event.getY();
                view.setX(x-(view.getWidth()/2));
                view.setY(y-(view.getHeight()/2));
                view.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        return true;    }
}
