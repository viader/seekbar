package koliczyna.seekbar.views;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 15.06.15.
 */
public interface OnCustomSeekBarChangeListener {
    void onProgressChanged(CustomSeekBarProgress progress);
}
