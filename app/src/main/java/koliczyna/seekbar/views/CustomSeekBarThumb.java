package koliczyna.seekbar.views;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 15.06.15.
 */
public class CustomSeekBarThumb {
    private int width;
    private int height;
    private int position;

    private int minPosition;
    private int maxPosition;
    private int mediumPosition;
    private CustomSeekBarProgress progress;

    private boolean pushed = false;

    public CustomSeekBarThumb(int width, int height, int seekBarWidth) {
        this.width = width;
        this.height = height;
        minPosition = width/2;
        mediumPosition = seekBarWidth/2;
        maxPosition = seekBarWidth-width/2;

        position = minPosition;
    }

    public void draw(Canvas canvas, Paint paint) {
        canvas.drawRect(position-width/2, 0, position + width/2,
                height, paint);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int newPosition) {
        this.position = newPosition;
        if(position>maxPosition)
            position=maxPosition;
        if(position<minPosition)
            position=minPosition;
    }

    public void setPosition(CustomSeekBarProgress state) {
        switch (state) {
            case MIN:
                position=minPosition;
                break;
            case MEDIUM:
                position=mediumPosition;
                break;
            case MAX:
                position=maxPosition;
                break;
        }
    }

    public CustomSeekBarProgress setProgressAndPosition() {
        if(mediumPosition-position >= 0) {
            if(position-minPosition < mediumPosition-position)
                progress = CustomSeekBarProgress.MIN;
            else
                progress = CustomSeekBarProgress.MEDIUM;
        }
        else {
            if(position-mediumPosition < maxPosition-position)
                progress = CustomSeekBarProgress.MEDIUM;
            else
                progress = CustomSeekBarProgress.MAX;
        }
        setPosition(progress);
        return progress;
    }

    public boolean isPushed() {
        return pushed;
    }

    public void setPushed(boolean pushed) {
        this.pushed = pushed;
    }

    public CustomSeekBarProgress getProgress() {
        return progress;
    }

    public void setProgress(CustomSeekBarProgress progress) {
        this.progress = progress;
        setPosition(progress);
    }
}
